<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class authenticationController extends Controller
{

  public function checkToken($user_id,$token){
    $check = DB::table('users_credential')->where('id',$user_id)->where('token',$token)->get();
    if(sizeof($check)<=0){
      $RESPONSE['tokenExpired'] = true;
      $RESPONSE['message']='Session Expired';
      $RESPONSE['error']=true;
      return $RESPONSE;
    }
  }
  
  public function userLogin(Request $request){
   $userPhone = $request->phone;
   $userPassword = $request->password;
   
   $checkUser = DB::table('users')->where('phone',$userPhone)->get();
   if(sizeof($checkUser)>0){
       $verifyPass = DB::table('users_credential')->where(['phone'=>$userPhone, 'password'=>md5($userPassword)])->get();
       if(sizeof($verifyPass)>0){
           $token = md5(rand());
           $updateToken = DB::table('users_credential')->where('phone',$userPhone)->update(['token'=>$token]);
           $getUserData = DB::table('users_credential')->where('users_credential.phone',$userPhone)->rightJoin('users','users_credential.id','=','users.id')->select('users.*','users_credential.token')->get();
           
           $RESPONSE['message']='user data success';
           $RESPONSE['error'] = false;
           $RESPONSE['data']= $getUserData;
       }
       else{
           $RESPONSE['message']='Password Wrong.';
           $RESPONSE['error'] = true;
       }
   }
   else{
           $RESPONSE['message']='User data not available.';
           $RESPONSE['error'] = true;
   }
   return $RESPONSE;
  }
  
  public function userData(Request $request){
      $authorization = $request->header('Authorization');
      $user_id = $request->header('userId');
      $token = explode(' ', $authorization);
      
      $RESPONSE = $this->checkToken($user_id,$token[1]);
      if($RESPONSE['tokenExpired']==true){
         return $RESPONSE;
      }
      else{
            $getUsers = DB::table('users')->select('*')->get();
            $RESPONSE['message']='Users List';
            $RESPONSE['error'] = false;
            $RESPONSE['data'] = $getUsers;
      }
     return $RESPONSE;
  }
  
}